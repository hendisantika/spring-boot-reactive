package com.hendisantika.springbootreactive.service;

import com.hendisantika.springbootreactive.model.Aircraft;
import com.hendisantika.springbootreactive.model.Flight;
import com.hendisantika.springbootreactive.repository.AircraftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 21.55
 * To change this template use File | Settings | File Templates.
 */

@Service
public class FlightService {
    @Autowired
    AircraftRepository repository;
    @Value("${opensky.base_url}")
    private String baseURL;
    @Value("${opensky.all_states}")
    private String allStates;

    @Bean
    WebClient client() {
        return WebClient.create(baseURL);
    }

    public Mono<Flight> getAllFlights() {
        return client().get().uri(allStates).accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(cr -> cr.bodyToMono(Flight.class));

    }

    public Mono<Aircraft> getFlightDetail(String icao24) {
        return repository.findByIcao(icao24);
    }


}
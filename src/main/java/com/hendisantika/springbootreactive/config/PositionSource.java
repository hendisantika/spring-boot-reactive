package com.hendisantika.springbootreactive.config;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 21.46
 * To change this template use File | Settings | File Templates.
 */
public enum PositionSource {
    ADS_B(0),
    ASTERIX(1),
    MLAT(2);

    private final int number;

    PositionSource(final int number) {
        this.number = number;
    }

    @JsonValue
    int getNumber() {
        return this.number;
    }
}

package com.hendisantika.springbootreactive.config;

import com.hendisantika.springbootreactive.model.Aircraft;
import com.hendisantika.springbootreactive.repository.AircraftRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 21.49
 * To change this template use File | Settings | File Templates.
 */


@Component
public class DataLoader implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);
    @Autowired
    AircraftRepository repository;
    List<Aircraft> aircrafts = new ArrayList();
    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public void run(String... args) throws Exception {
        Resource resource = resourceLoader.getResource("classpath:aircraftDatabase.csv");
        LOGGER.info("Start loading data ...");
        InputStream is = resource.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        CSVParser parser = new CSVParser(br, CSVFormat.EXCEL.withHeader().withTrim());

        Iterable<CSVRecord> records = parser.getRecords();
        records.forEach(r -> {
            if (!r.get("icao24").isEmpty())
                aircrafts.add(
                        new Aircraft(r.get("icao24"),
                                r.get("registration"),
                                r.get("manufacturericao"),
                                r.get("manufacturername"),
                                r.get("model"),
                                r.get("owner"),
                                r.get("operator"),
                                r.get("reguntil"),
                                r.get("engines"),
                                r.get("built")
                        ));
        });

        repository.saveAll(aircrafts).subscribe(v -> LOGGER.info("saving {}", v), e -> LOGGER.error("Saving Failed", e), () -> LOGGER.info("Loading Data Complete!"));
    }
}

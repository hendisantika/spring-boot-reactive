package com.hendisantika.springbootreactive.repository;

import com.hendisantika.springbootreactive.model.Aircraft;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 21.50
 * To change this template use File | Settings | File Templates.
 */
public interface AircraftRepository extends ReactiveCrudRepository<Aircraft, Long> {

    Mono<Aircraft> findByIcao(String icao);
}

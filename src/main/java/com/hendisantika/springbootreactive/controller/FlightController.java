package com.hendisantika.springbootreactive.controller;

import com.hendisantika.springbootreactive.model.Aircraft;
import com.hendisantika.springbootreactive.model.Flight;
import com.hendisantika.springbootreactive.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 21.57
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class FlightController {
    @Autowired
    FlightService service;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE, value = "/flights")
    @ResponseBody
    Flux<Flight> flights() {
        return Flux.from(service.getAllFlights());

    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/aircraft/{icao}")
    @ResponseBody
    Mono<Aircraft> aircraft(@PathVariable String icao) {
        return service.getFlightDetail(icao);

    }

    @GetMapping("/")
    Mono<String> home() {
        return Mono.just("flights");
    }
}
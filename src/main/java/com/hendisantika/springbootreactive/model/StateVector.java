package com.hendisantika.springbootreactive.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hendisantika.springbootreactive.config.PositionSource;
import lombok.Data;

import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 21.46
 * To change this template use File | Settings | File Templates.
 */

@Data
@JsonFormat(shape = JsonFormat.Shape.ARRAY)
public class StateVector {
    private String icao24;
    private String callsign;
    private String originCountry;
    private Double lastPositionUpdate;
    private Double lastContact;
    private Double longitude;
    private Double latitude;
    private Double geoAltitude;
    private boolean onGround;
    private Double velocity;
    private Double heading;
    private Double verticalRate;
    private Set<Integer> serials;
    private Double baroAltitude;
    private String squawk;
    private boolean spi;
    private PositionSource positionSource;


}

package com.hendisantika.springbootreactive.model;

import lombok.Data;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 21.45
 * To change this template use File | Settings | File Templates.
 */

@Data
public class Flight {
    private int time;
    private Collection<StateVector> states;
}
